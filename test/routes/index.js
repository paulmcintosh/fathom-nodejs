var express = require('express');
var router = express.Router();
var services = require('../services');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Form Validation', success: req.session.success, errors: req.session.errors});
  req.session.errors = null;
});

/* GET create page. */
router.get('/create', function(req, res, next) {
  res.render('create', { title: 'Create', condition: true, records:services.records});
});

/* GET read page. */
router.get('/read', function(req, res, next) {
  services.setId(req,services);
  services.find(services);
  res.render('read', { title: 'Read', condition: true, records:services.records,id:services.id,limit:services.limit});
});

/* GET read id page. */
router.get('/read/:id',function(req, res, next){
  services.setId(req,services);
  services.find(services);
  res.render('read',{title: 'Read', condition: true, records:services.records,id:services.id,limit:services.limit});
});

/* GET read id page. */
router.get('/read/:id/:limit',function(req, res, next){
  services.setId(req,services);
  services.find(services);
  res.render('read',{title: 'Read', condition: true, records:services.records,id:services.id,limit:services.limit});
});

/* GET details id page. */
router.get('/details/:id/:limit',function(req, res, next){
  services.setId(req,services);
  services.find(services);
  services.setNext(services);
  console.log(services);
  res.render('details',{title: 'Details', condition: true, records:services.records,id:services.id,limit:services.limit,next:services.next});
});

/* GET update page. */
router.get('/update', function(req, res, next) {
  res.render('update', { title: 'Update', condition: true, records:services.records});
});

/* GET delete page. */
router.get('/delete', function(req, res, next) {
  res.render('delete', { title: 'Delete', condition: true, records:services.records});
});


router.post('/submit',function(req, res, next){
  req.check('email','Invalid email').isEmail();
  req.check('password','Password is invalid').isLength({min: 4}).equals(req.body.confirmPassword);
  var errors = req.validationErrors();
  if(errors){
    req.session.errors = errors;
    req.session.success = false;
  } else {
    req.session.success = true;
  }
  res.redirect('/');
});


module.exports = router;
